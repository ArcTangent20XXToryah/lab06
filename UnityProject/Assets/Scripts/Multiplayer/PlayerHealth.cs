﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerHealth : NetworkBehaviour
{
    int logger = 120;
    public int health = 100;
    private int previousHealth;
    [SyncVar]
    int heals;
    // Use this for initialization
    void Start()
    {
        health = 100;
        previousHealth = health;
    }

    // Update is called once per frame
    
    public void Heal()
    {
        health++;
        Debug.Log("Took Health");
        TransmitHealth();
    }

    public void Dmg()
    {
        health--;
        Debug.Log("Took Damage");
        TransmitHealth();
    }
    void Update()
    {
        //TransmitHealth();
        if (logger == 0)
        {
            Debug.Log(health);
            logger = 120;
        }
        else
        {
            logger--;
        }
    }

    [Client]
    void TransmitHealth()
    {
        Debug.Log("Called");
            Debug.Log("Health Updated");
            CmdSendHealthToServer(health);
            previousHealth = health;
    }

    [Command]
    void CmdSendHealthToServer(int health)
    {
        heals = health;
    }

    
}
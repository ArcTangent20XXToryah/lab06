﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Name : MonoBehaviour
{
    public static Name Instance { get; private set; }
    public string playerName;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        Instance = this;

        DontDestroyOnLoad(this.gameObject);
    }
    // Use this for initialization
    void Start()
    {
        GameObject.Find("PlayerName").GetComponent<Text>().text = playerName;
    }

    // Update is called once per frame
    void Update()
    {
            
    }
}

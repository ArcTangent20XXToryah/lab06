public void UpdateName(Object message)
    {
        Text textObj = ((GameObject)message).GetComponent<Text>();
        if (textObj.text == "")
            GameObject.Find("Network Manager").GetComponent<Name>().playerName = "Player";
        else
            GameObject.Find("Network Manager").GetComponent<Name>().playerName = textObj.text;
        Debug.Log("Updating name to... " + textObj.text);
    }